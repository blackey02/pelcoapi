﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestTriggeredRecord
{
    public partial class Form1 : Form
    {
        string _sessionId = String.Empty;
        
        public Form1()
        {
            InitializeComponent();

            txtDeviceIP.Text = "192.168.5.189";
            txtDevicePort.Text = "60010";
            txtDeviceId.Text = "114";

            txtNvrIP.Text = "192.168.5.189";
            txtNvrPort.Text = "60002";
            txtNvrId.Text = "1";
        }

        private void btnStartRecording_Click(object sender, EventArgs e)
        {
            try
            {
                btnStartRecording.Enabled = false;
                var deviceIp = txtDeviceIP.Text;
                var devicePort = txtDevicePort.Text;
                var deviceId = txtDeviceId.Text;
                var nvrIp = txtNvrIP.Text;
                var nvrPort = txtNvrPort.Text;
                var nvrId = txtNvrId.Text;

                System.Threading.ThreadPool.QueueUserWorkItem(obj =>
                    {
                        while (true)
                        {
                            int asyncId = 0;
                            bool asyncIdSpecified = false;

                            //query camera for available stream
                            VideoOutputRef.VideoOutput videoOutput = new TestTriggeredRecord.VideoOutputRef.VideoOutput();
                            videoOutput.Url = "http://" + deviceIp + ":" + devicePort + "/control/VideoOutput-" + deviceId;
                            {
                                System.Net.ServicePointManager.FindServicePoint(new Uri(videoOutput.Url)).Expect100Continue = false;
                            }

                            VideoOutputRef.StreamCatalog availableStreams;
                            VideoOutputRef.StreamParameters chosenStream;
                            VideoInputRef.StreamParameters inputStream;

                            //create input service proxy, Connect monitor to camera's stream
                            VideoInputRef.VideoInput videoInput = new TestTriggeredRecord.VideoInputRef.VideoInput();
                            videoInput.Url = "http://" + nvrIp + ":" + nvrPort + "/control/VideoInput-" + nvrId;
                            {
                                System.Net.ServicePointManager.FindServicePoint(new Uri(videoInput.Url)).Expect100Continue = false;
                            }

                            availableStreams = videoOutput.Query(new TestTriggeredRecord.VideoOutputRef.StreamQuery(), ref asyncId, ref asyncIdSpecified);
                            chosenStream = availableStreams.entries[0];
                            videoOutput.Connect(ref chosenStream, ref asyncId, ref asyncIdSpecified);

                            inputStream = new TestTriggeredRecord.VideoInputRef.StreamParameters();

                            inputStream.streamSession = new TestTriggeredRecord.VideoInputRef.StreamSession();
                            inputStream.compatability = new TestTriggeredRecord.VideoInputRef.Compatibility();
                            inputStream.compatability.resolution = new TestTriggeredRecord.VideoInputRef.Resolution();
                            if (chosenStream.compatability != null)
                            {
                                switch (chosenStream.compatability.resolution)
                                {
                                    case "CIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.CIF;
                                        break;
                                    case "2CIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.Item2CIF;
                                        break;
                                    case "4CIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.Item4CIF;
                                        break;
                                    case "SIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.SIF;
                                        break;
                                    case "2SIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.Item2SIF;
                                        break;
                                    case "4SIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.Item4SIF;
                                        break;
                                    case "QCIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.QCIF;
                                        break;
                                    case "QSIF": inputStream.compatability.resolution.Value = TestTriggeredRecord.VideoInputRef.RESOLUTIONS.QSIF;
                                        break;

                                }
                            }

                            inputStream.streamSession.outputURI = chosenStream.streamSession.sourceURI;
                            inputStream.streamSession.refreshInterval = chosenStream.streamSession.refreshInterval;

                            inputStream.videoQuality = new TestTriggeredRecord.VideoInputRef.VideoQuality();
                            inputStream.videoQuality.bitrate = chosenStream.videoQuality.bitrate;
                            inputStream.videoQuality.format = chosenStream.videoQuality.format;
                            inputStream.videoQuality.framerate = (double)chosenStream.videoQuality.framerate;
                            inputStream.videoQuality.tvformat = chosenStream.videoQuality.tvformat;
                            inputStream.videoQuality.videosize = chosenStream.videoQuality.videosize;
                            inputStream.videoQuality.gop = chosenStream.videoQuality.gop;

                            //Trying to set the endtime for the manual recording produces a SoapException
                            //DateTime current = DateTime.Now.ToUniversalTime();
                            //string endTime = current.Year + "-" + current.Month + "-" + current.Day + "T" + current.Hour + ":" + current.Minute + ":" + (current.Second + 5);
                            //inputStream.streamSession.endTime = endTime;

                            //This has no effect on end time or stopping the recording
                            //inputStream.preAlarmDurationInManualRecord = 5; 

                            //Setting session ID manually produces SoapException
                            //inputStream.streamSession.sessionId = "1111"; 

                            videoInput.Connect(ref inputStream);

                            _sessionId = inputStream.streamSession.sessionId;
                            if (_sessionId == null || _sessionId == string.Empty)
                            {
                                MessageBox.Show("Connect request failed");
                                BeginInvoke(new Action(() => { btnStartRecording.Enabled = true; btnStopRecording.Enabled = false; }));
                                return;
                            }
                            else
                                BeginInvoke(new Action(() => btnStopRecording.Enabled = true));

                            System.Diagnostics.Trace.WriteLine(string.Format("{0} - Connect Complete", DateTime.Now));
                            System.Threading.Thread.Sleep(1000 * 60 * 30);
                            System.Diagnostics.Trace.WriteLine(string.Format("{0} - Connect Starting", DateTime.Now));
                        }
                    });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void btnStopRecording_Click(object sender, EventArgs e)
        {
            try
            {
                btnStartRecording.Enabled = true;
                btnStopRecording.Enabled = false;

                VideoInputRef.VideoInput videoInput = new TestTriggeredRecord.VideoInputRef.VideoInput();
                videoInput.Url = "http://" + txtNvrIP.Text + ":" + txtNvrPort.Text + "/control/VideoInput-" + txtNvrId.Text;
                {
                    System.Net.ServicePointManager.FindServicePoint(new Uri(videoInput.Url)).Expect100Continue = false;
                }

                //string[] temp = videoInput.GetActiveSessionIds();
                //sessionId = "3705";
                videoInput.Disconnect(_sessionId);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
