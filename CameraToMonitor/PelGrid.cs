﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Example
{
    public partial class PelGrid : UserControl
    {
        public event DragEventHandler ItemDropped;
        private enum LayoutGrid { Unknown = -1, _1x1, _2x2, _3x3, _4x4 }
        private LayoutGrid _currentGrid = LayoutGrid.Unknown;

        public PelGrid()
        {
            InitializeComponent();
            Clear();
        }

        public int Layout 
        {
            get
            {
                return (int)_currentGrid;
            } 
        }

        public void Layout1x1()
        {
            Clear();
            panel1.BackColor = System.Drawing.Color.LightGray;
            _currentGrid = LayoutGrid._1x1;
        }

        public void Layout2x2()
        {
            Layout1x1();
            panel2.BackColor = System.Drawing.Color.LightGray;
            panel5.BackColor = System.Drawing.Color.LightGray;
            panel6.BackColor = System.Drawing.Color.LightGray;
            _currentGrid = LayoutGrid._2x2;
        }

        public void Layout3x3()
        {
            Layout2x2();
            panel3.BackColor = System.Drawing.Color.LightGray;
            panel7.BackColor = System.Drawing.Color.LightGray;
            panel9.BackColor = System.Drawing.Color.LightGray;
            panel10.BackColor = System.Drawing.Color.LightGray;
            panel11.BackColor = System.Drawing.Color.LightGray;
            _currentGrid = LayoutGrid._3x3;
        }

        public void Layout4x4()
        {
            Layout3x3();
            panel4.BackColor = System.Drawing.Color.LightGray;
            panel8.BackColor = System.Drawing.Color.LightGray;
            panel12.BackColor = System.Drawing.Color.LightGray;
            panel13.BackColor = System.Drawing.Color.LightGray;
            panel14.BackColor = System.Drawing.Color.LightGray;
            panel15.BackColor = System.Drawing.Color.LightGray;
            panel16.BackColor = System.Drawing.Color.LightGray;
            _currentGrid = LayoutGrid._4x4;
        }

        public int ChannelInActiveLayout(Panel panel)
        { 
            var cell = tableLayoutPanel1.GetCellPosition(panel);
            return (cell.Row * (Layout+1)) + cell.Column; 
        }

        private void Clear()
        {
            foreach (Control panel in tableLayoutPanel1.Controls)
            {
                if(panel is Panel)
                    panel.BackColor = System.Drawing.Color.Gray;
            }
        }

        private void Panel_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            var panel = sender as Panel;
            if (panel.BackColor == System.Drawing.Color.LightGray)
                e.Effect = DragDropEffects.Move;
        }

        private void Panel_DragDrop(object sender, DragEventArgs e)
        {
            FireItemDropped(sender, e);
        }

        private void FireItemDropped(object sender, DragEventArgs e)
        {
            if(ItemDropped != null)
                ItemDropped(sender, e);
        }
    }
}
