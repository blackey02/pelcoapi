﻿namespace Example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxMonitors = new System.Windows.Forms.ListBox();
            this.listBoxCameras = new System.Windows.Forms.ListBox();
            this.button1x1 = new System.Windows.Forms.Button();
            this.button2x2 = new System.Windows.Forms.Button();
            this.button3x3 = new System.Windows.Forms.Button();
            this.button4x4 = new System.Windows.Forms.Button();
            this.buttonGetDevices = new System.Windows.Forms.Button();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPasswd = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.pelGrid1 = new Example.PelGrid();
            this.SuspendLayout();
            // 
            // listBoxMonitors
            // 
            this.listBoxMonitors.FormattingEnabled = true;
            this.listBoxMonitors.Location = new System.Drawing.Point(12, 21);
            this.listBoxMonitors.Name = "listBoxMonitors";
            this.listBoxMonitors.Size = new System.Drawing.Size(172, 316);
            this.listBoxMonitors.TabIndex = 0;
            // 
            // listBoxCameras
            // 
            this.listBoxCameras.FormattingEnabled = true;
            this.listBoxCameras.Location = new System.Drawing.Point(190, 21);
            this.listBoxCameras.Name = "listBoxCameras";
            this.listBoxCameras.Size = new System.Drawing.Size(172, 316);
            this.listBoxCameras.TabIndex = 1;
            this.listBoxCameras.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Cameras_MouseDown);
            // 
            // button1x1
            // 
            this.button1x1.Location = new System.Drawing.Point(394, 388);
            this.button1x1.Name = "button1x1";
            this.button1x1.Size = new System.Drawing.Size(75, 23);
            this.button1x1.TabIndex = 3;
            this.button1x1.Text = "1x1";
            this.button1x1.UseVisualStyleBackColor = true;
            this.button1x1.Click += new System.EventHandler(this.Button1x1_Click);
            // 
            // button2x2
            // 
            this.button2x2.Location = new System.Drawing.Point(491, 388);
            this.button2x2.Name = "button2x2";
            this.button2x2.Size = new System.Drawing.Size(75, 23);
            this.button2x2.TabIndex = 4;
            this.button2x2.Text = "2x2";
            this.button2x2.UseVisualStyleBackColor = true;
            this.button2x2.Click += new System.EventHandler(this.Button2x2_Click);
            // 
            // button3x3
            // 
            this.button3x3.Location = new System.Drawing.Point(589, 388);
            this.button3x3.Name = "button3x3";
            this.button3x3.Size = new System.Drawing.Size(75, 23);
            this.button3x3.TabIndex = 5;
            this.button3x3.Text = "3x3";
            this.button3x3.UseVisualStyleBackColor = true;
            this.button3x3.Click += new System.EventHandler(this.Button3x3_Click);
            // 
            // button4x4
            // 
            this.button4x4.Location = new System.Drawing.Point(685, 388);
            this.button4x4.Name = "button4x4";
            this.button4x4.Size = new System.Drawing.Size(75, 23);
            this.button4x4.TabIndex = 6;
            this.button4x4.Text = "4x4";
            this.button4x4.UseVisualStyleBackColor = true;
            this.button4x4.Click += new System.EventHandler(this.Button4x4_Click);
            // 
            // buttonGetDevices
            // 
            this.buttonGetDevices.Location = new System.Drawing.Point(12, 343);
            this.buttonGetDevices.Name = "buttonGetDevices";
            this.buttonGetDevices.Size = new System.Drawing.Size(95, 23);
            this.buttonGetDevices.TabIndex = 7;
            this.buttonGetDevices.Text = "Get Devices";
            this.buttonGetDevices.UseVisualStyleBackColor = true;
            this.buttonGetDevices.Click += new System.EventHandler(this.GetDevices_Click);
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(12, 388);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(83, 20);
            this.textBoxIp.TabIndex = 9;
            this.textBoxIp.Text = "192.168.5.10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 372);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "SM IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 372);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "SM Port";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(101, 388);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(52, 20);
            this.textBoxPort.TabIndex = 12;
            this.textBoxPort.Text = "60001";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 413);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "SM Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 413);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "SM Passwd";
            // 
            // textBoxPasswd
            // 
            this.textBoxPasswd.Location = new System.Drawing.Point(101, 429);
            this.textBoxPasswd.Name = "textBoxPasswd";
            this.textBoxPasswd.Size = new System.Drawing.Size(63, 20);
            this.textBoxPasswd.TabIndex = 16;
            this.textBoxPasswd.Text = "admin";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(14, 429);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(63, 20);
            this.textBoxUsername.TabIndex = 17;
            this.textBoxUsername.Text = "admin";
            // 
            // pelGrid1
            // 
            this.pelGrid1.AutoSize = true;
            this.pelGrid1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pelGrid1.Location = new System.Drawing.Point(368, 21);
            this.pelGrid1.Name = "pelGrid1";
            this.pelGrid1.Size = new System.Drawing.Size(463, 358);
            this.pelGrid1.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 456);
            this.Controls.Add(this.pelGrid1);
            this.Controls.Add(this.textBoxUsername);
            this.Controls.Add(this.textBoxPasswd);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIp);
            this.Controls.Add(this.buttonGetDevices);
            this.Controls.Add(this.button4x4);
            this.Controls.Add(this.button3x3);
            this.Controls.Add(this.button2x2);
            this.Controls.Add(this.button1x1);
            this.Controls.Add(this.listBoxCameras);
            this.Controls.Add(this.listBoxMonitors);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxMonitors;
        private System.Windows.Forms.ListBox listBoxCameras;
        private System.Windows.Forms.Button button1x1;
        private System.Windows.Forms.Button button2x2;
        private System.Windows.Forms.Button button3x3;
        private System.Windows.Forms.Button button4x4;
        private System.Windows.Forms.Button buttonGetDevices;
        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPasswd;
        private System.Windows.Forms.TextBox textBoxUsername;
        private PelGrid pelGrid1;

    }
}

