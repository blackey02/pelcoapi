﻿namespace Example
{
    partial class PelGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel16, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel15, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel14, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel13, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel11, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel10, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(460, 352);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(109, 82);
            this.panel1.TabIndex = 16;
            this.panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel1.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel16
            // 
            this.panel16.AllowDrop = true;
            this.panel16.Location = new System.Drawing.Point(348, 267);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(109, 82);
            this.panel16.TabIndex = 15;
            this.panel16.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel16.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel15
            // 
            this.panel15.AllowDrop = true;
            this.panel15.Location = new System.Drawing.Point(233, 267);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(109, 82);
            this.panel15.TabIndex = 14;
            this.panel15.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel15.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel14
            // 
            this.panel14.AllowDrop = true;
            this.panel14.Location = new System.Drawing.Point(118, 267);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(109, 82);
            this.panel14.TabIndex = 13;
            this.panel14.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel14.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel13
            // 
            this.panel13.AllowDrop = true;
            this.panel13.Location = new System.Drawing.Point(3, 267);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(109, 82);
            this.panel13.TabIndex = 12;
            this.panel13.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel13.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel12
            // 
            this.panel12.AllowDrop = true;
            this.panel12.Location = new System.Drawing.Point(348, 179);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(109, 82);
            this.panel12.TabIndex = 11;
            this.panel12.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel12.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel11
            // 
            this.panel11.AllowDrop = true;
            this.panel11.Location = new System.Drawing.Point(233, 179);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(109, 82);
            this.panel11.TabIndex = 10;
            this.panel11.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel11.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel10
            // 
            this.panel10.AllowDrop = true;
            this.panel10.Location = new System.Drawing.Point(118, 179);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(109, 82);
            this.panel10.TabIndex = 9;
            this.panel10.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel10.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel9
            // 
            this.panel9.AllowDrop = true;
            this.panel9.Location = new System.Drawing.Point(3, 179);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(109, 82);
            this.panel9.TabIndex = 8;
            this.panel9.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel9.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel8
            // 
            this.panel8.AllowDrop = true;
            this.panel8.Location = new System.Drawing.Point(348, 91);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(109, 82);
            this.panel8.TabIndex = 7;
            this.panel8.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel8.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel7
            // 
            this.panel7.AllowDrop = true;
            this.panel7.Location = new System.Drawing.Point(233, 91);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(109, 82);
            this.panel7.TabIndex = 6;
            this.panel7.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel7.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel6
            // 
            this.panel6.AllowDrop = true;
            this.panel6.Location = new System.Drawing.Point(118, 91);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(109, 82);
            this.panel6.TabIndex = 5;
            this.panel6.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel6.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel5
            // 
            this.panel5.AllowDrop = true;
            this.panel5.Location = new System.Drawing.Point(3, 91);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(109, 82);
            this.panel5.TabIndex = 4;
            this.panel5.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel5.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel4
            // 
            this.panel4.AllowDrop = true;
            this.panel4.Location = new System.Drawing.Point(348, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(109, 82);
            this.panel4.TabIndex = 3;
            this.panel4.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel4.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel3
            // 
            this.panel3.AllowDrop = true;
            this.panel3.Location = new System.Drawing.Point(233, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(109, 82);
            this.panel3.TabIndex = 2;
            this.panel3.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel3.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // panel2
            // 
            this.panel2.AllowDrop = true;
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.Location = new System.Drawing.Point(118, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(109, 82);
            this.panel2.TabIndex = 1;
            this.panel2.DragDrop += new System.Windows.Forms.DragEventHandler(this.Panel_DragDrop);
            this.panel2.DragOver += new System.Windows.Forms.DragEventHandler(this.Panel_DragOver);
            // 
            // PelGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PelGrid";
            this.Size = new System.Drawing.Size(463, 358);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
    }
}
