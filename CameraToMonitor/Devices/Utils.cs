﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Example.Devices
{
    static class Utils
    {
        public static string ControlUrl(string ddfUri, string serviceId)
        {
            var match = Regex.Match(ddfUri, @"(?<=//)[\d.]+");
            string ip = match.Value;
            match = Regex.Match(ddfUri, @"(?<=:)[\d]+");
            string port = match.Value;
            match = Regex.Match(serviceId, @"(?<=:)[\w-]+$");
            string serviceIdEnding = match.Value;
            string controlUrl = string.Format("http://{0}:{1}/control/{2}", ip, port, serviceIdEnding);
            return controlUrl;
        }
    }
}
