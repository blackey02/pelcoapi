﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example.Devices
{
    class Monitor
    {
        DeviceRegister.DeviceInfo _monitor;
        MonitorConfigurationNs.MonitorConfiguration _monitorConfiguration;
        VideoInputNs.VideoInput _videoInput;

        public Monitor(DeviceRegister.DeviceInfo monitor)
        {
            _monitor = monitor;
            string ddfUri = _monitor.Attributes["SYS_UpnpDevDescUrl"];
            _monitorConfiguration = new MonitorConfigurationNs.MonitorConfiguration();
            string serviceId = _monitor.Services.FirstOrDefault(info => info.Type == "urn:schemas-pelco-com:service:MonitorConfiguration:1").Id;
            _monitorConfiguration.Url = Utils.ControlUrl(ddfUri, serviceId);
            _videoInput = new VideoInputNs.VideoInput();
            serviceId = _monitor.Services.FirstOrDefault(info => info.Type == "urn:schemas-pelco-com:service:VideoInput:1").Id;
            _videoInput.Url = Utils.ControlUrl(ddfUri, serviceId);
        }

        public void SetLayout(int layout)
        {
            _monitorConfiguration.SetLayout(layout);
        }

        public void SetActiveChannel(int channel)
        {
            _monitorConfiguration.SetActiveChannel(channel);
        }

        public void Connect(Camera camera)
        {
            var parameters = camera.Query();
            var inputParams = new VideoInputNs.StreamParameters();
            inputParams.preAlarmDurationInManualRecord = parameters.preAlarmDurationInManualRecord;

            string videoOutputUri = string.Format("{0}/{1}", camera.Uuid, camera.Services.First(info => info.Type == "urn:schemas-pelco-com:service:VideoOutput:1").Id);
            string streamControlUri = string.Format("{0}/{1}", camera.Uuid, camera.Services.First(info => info.Type == "urn:schemas-pelco-com:service:StreamControl:1").Id);
            inputParams.streamSession = new VideoInputNs.StreamSession()
            {
                sourceURI = videoOutputUri,
                outputURI = videoOutputUri,
                controlURI = streamControlUri,
                iFrameOnly = parameters.streamSession.iFrameOnly,
                secsPerIFrame = (uint)parameters.streamSession.secsPerIFrame,
                startTime = parameters.streamSession.startTime,
                endTime = parameters.streamSession.endTime,
                refreshInterval = parameters.streamSession.refreshInterval
            };
            inputParams.videoQuality = new VideoInputNs.VideoQuality() 
            { 
                framerate = (double)parameters.videoQuality.framerate,
                bitrate = parameters.videoQuality.bitrate,
                format = parameters.videoQuality.format,
                videosize = parameters.videoQuality.videosize,
                tvformat = parameters.videoQuality.tvformat,
                gop = parameters.videoQuality.gop
            };

            _videoInput.Connect(ref inputParams);
        }
    }
}
