﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example.Devices
{
    class Camera
    {
        DeviceRegister.DeviceInfo _camera;
        VideoOutputNs.VideoOutput _videoOutput;

        public Camera(DeviceRegister.DeviceInfo camera)
        {
            _camera = camera;
            string ddfUri = _camera.Attributes["SYS_UpnpDevDescUrl"];
            _videoOutput = new VideoOutputNs.VideoOutput();
            string serviceId = _camera.Services.FirstOrDefault(info => info.Type == "urn:schemas-pelco-com:service:VideoOutput:1").Id;
            _videoOutput.Url = Utils.ControlUrl(ddfUri, serviceId);
        }

        public string Uuid
        {
            get
            {
                return _camera.UDN;
            }
        }

        public List<DeviceRegister.ServiceInfo> Services
        {
            get
            {
                return _camera.Services;
            }
        }

        public VideoOutputNs.StreamParameters Query()
        { 
            int asyncId = 0;
            bool asyncIdSpecified = false;
            VideoOutputNs.StreamCatalog catalog = _videoOutput.Query(new VideoOutputNs.StreamQuery(), ref asyncId, ref asyncIdSpecified);
            return catalog.entries.Last();
        }
    }
}
