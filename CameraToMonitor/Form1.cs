﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Example.Devices;

namespace Example
{
    public partial class Form1 : Form
    {
        private DeviceRegister.SmCredentials _cred;
        Dictionary<string, DeviceRegister.DeviceInfo> _devices = new Dictionary<string, DeviceRegister.DeviceInfo>();

        public Form1()
        {
            InitializeComponent();
            System.Net.ServicePointManager.Expect100Continue = false;
            pelGrid1.ItemDropped += new DragEventHandler(PelGrid_ItemDropped);
        }

        private object MonitorSelected
        {
            get
            {
                if (InvokeRequired)
                {
                    object selectedMon = null;
                    Invoke(new MethodInvoker(() => selectedMon = MonitorSelected));
                    return selectedMon;
                }

                return listBoxMonitors.SelectedItem;
            }
        }

        private void PelGrid_ItemDropped(object sender, DragEventArgs e)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(obj =>
            {
                lock (this)
                {
                    try
                    {
                        var panel = sender as Panel;
                        var cameraItem = (DeviceRegister.DeviceInfo)e.Data.GetData(typeof(DeviceRegister.DeviceInfo));
                        var monitorItem = (DeviceRegister.DeviceInfo)MonitorSelected;
                        var monitor = new Monitor(monitorItem);
                        var camera = new Camera(cameraItem);
                        int channel = pelGrid1.ChannelInActiveLayout(panel);
                        monitor.SetActiveChannel(channel);
                        monitor.Connect(camera);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            });
        }

        private void GetDevices_Click(object sender, EventArgs e)
        {
            _cred = new DeviceRegister.SmCredentials() { Ip = textBoxIp.Text, Port = ushort.Parse(textBoxPort.Text), 
                Username = textBoxUsername.Text, Password = textBoxPasswd.Text };
            DeviceRegister.Register register = new DeviceRegister.Register(_cred);

            if (register.Open())
            {
                _devices = register.GetDevices(new DeviceRegister.DeviceType[] { DeviceRegister.DeviceType.Camera, DeviceRegister.DeviceType.Monitor });
                register.Close();
                PopulateDevices();
            }
            else
            {
                _cred = null;
                _devices.Clear();
            }
        }

        private void PopulateDevices()
        {
            listBoxCameras.Items.Clear();
            listBoxMonitors.Items.Clear();

            var cameraDevices = _devices.Values.Where(info => info.Attributes["SYS_UpnpDeviceType"] == "urn:schemas-pelco-com:device:Camera:1");
            var monitorDevices = _devices.Values.Where(info => info.Attributes["SYS_UpnpDeviceType"] == "urn:schemas-pelco-com:device:Monitor:1");
            foreach (var device in cameraDevices)
                listBoxCameras.Items.Add(device);
            foreach (var device in monitorDevices)
                listBoxMonitors.Items.Add(device);
        }

        private void Button1x1_Click(object sender, EventArgs e)
        {
            if (MonitorSelected == null) return;
            pelGrid1.Layout1x1();
            SetLayoutActiveMonitor();
        }

        private void Button2x2_Click(object sender, EventArgs e)
        {
            if (MonitorSelected == null) return;
            pelGrid1.Layout2x2();
            SetLayoutActiveMonitor();
        }

        private void Button3x3_Click(object sender, EventArgs e)
        {
            if (MonitorSelected == null) return;
            pelGrid1.Layout3x3();
            SetLayoutActiveMonitor();
        }

        private void Button4x4_Click(object sender, EventArgs e)
        {
            if (MonitorSelected == null) return;
            pelGrid1.Layout4x4();
            SetLayoutActiveMonitor();
        }

        private void SetLayoutActiveMonitor()
        {
            if (MonitorSelected != null)
            {
                var monitor = new Monitor((DeviceRegister.DeviceInfo)MonitorSelected);
                monitor.SetLayout(pelGrid1.Layout);
            }
        }

        private void Cameras_MouseDown(object sender, MouseEventArgs e)
        {
            if (listBoxCameras.SelectedItem == null) return;
            listBoxCameras.DoDragDrop(listBoxCameras.SelectedItem, DragDropEffects.Move);
        }
    }
}
