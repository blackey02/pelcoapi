﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PtzControl
{
    class Program
    {
        // User configurable options //////////
        // Camera socket to use
        private const string CAM_SOCKET = "10.221.224.110:80";

        // Camera credentials (if closed authentication)
        private static readonly string CAM_USERNAME = string.Empty;
        private static readonly string CAM_PASSWORD = string.Empty;

        // Preset and pattern number to use
        private const uint PRESET_PATTERN = 8;
        ///////////////////////////////////////
        
        private const int ONE_SECOND_MS = 1000;
        private static PTZFacade _ptz;

        private static void Main(string[] args)
        {
            Console.WriteLine("========== PtzControl ==========");
            Console.WriteLine();
            Console.WriteLine(string.Format("Enter a Sarix camera socket, or press enter for the default. \n\tDefault: '{0}'", CAM_SOCKET));
            string username = string.Empty;
            string password = string.Empty;
            string customCamera = Console.ReadLine().Trim();
            if (string.IsNullOrWhiteSpace(customCamera))
            {
                customCamera = CAM_SOCKET;
                username = CAM_USERNAME;
                password = CAM_PASSWORD;
            }
            else
            {
                Console.WriteLine(string.Format("Enter the Sarix camera username, or press enter for the default. \n\tDefault: '{0}'", CAM_USERNAME));
                username = Console.ReadLine().Trim();
                Console.WriteLine(string.Format("Enter the Sarix camera password, or press enter for the default. \n\tDefault: '{0}'", CAM_PASSWORD));
                password = Console.ReadLine().Trim();
            }

            try
            {
                _ptz = new PTZFacade(customCamera, username, password);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine();
                Console.WriteLine("Press Enter To Exit");
                Console.ReadLine();
                return;
            }

            Console.WriteLine("ExercisePanTilt");
            ExercisePanTilt();
            Thread.Sleep(ONE_SECOND_MS);
            Console.WriteLine("ExercisePresets");
            ExercisePresets();
            Thread.Sleep(ONE_SECOND_MS);
            Console.WriteLine("ExercisePatterns");
            ExercisePatterns();
            Thread.Sleep(ONE_SECOND_MS);
            Console.WriteLine("ExerciseFocus");
            ExerciseFocus();
            Thread.Sleep(ONE_SECOND_MS);
            Console.WriteLine("ExerciseIris");
            ExerciseIris();
            Thread.Sleep(ONE_SECOND_MS);
            Console.WriteLine("DisplaySettings");
            _ptz.DisplaySettings();

            Console.WriteLine();
            Console.WriteLine("Press Enter To Exit");
            Console.ReadLine();
        }

        /// <summary>
        /// Exercise the camera's actions relating to pan, tilt
        /// </summary>
        private static void ExercisePanTilt()
        {
            _ptz.Move(PTZFacade.MoveDirection.Up);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.UpRight);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.Right);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.DownRight);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.Down);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.DownLeft);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.Left);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.UpLeft);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.StopMoving();
        }

        /// <summary>
        /// Exercise the camera's actions relating to presets
        /// </summary>
        private static void ExercisePresets()
        {
            _ptz.SetPreset(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.Right);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.StopMoving();
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.ExecutePreset(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.RemovePreset(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Move(PTZFacade.MoveDirection.Right);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.StopMoving();
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.ExecutePreset(PRESET_PATTERN);
        }

        /// <summary>
        /// Exercise the camera's actions relating to patterns
        /// </summary>
        private static void ExercisePatterns()
        {
            _ptz.StartRecordPattern(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            ExercisePanTilt();
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.StopRecordPattern(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.ExecutePattern(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS * 10);
            _ptz.StopPattern(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.RemovePattern(PRESET_PATTERN);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.ExecutePattern(PRESET_PATTERN);
        }

        /// <summary>
        /// Exercise the camera's actions relating to focus, zoom
        /// </summary>
        private static void ExerciseFocus()
        {
            float zoomMag = _ptz.ZoomMagnification();
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.SetZoomMagnification(1);
            Thread.Sleep(ONE_SECOND_MS * 3);
            _ptz.SetAutoFocus(PTZFacade.AutoFocus.Off);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.SetZoomMagnification(10);
            Thread.Sleep(ONE_SECOND_MS * 10);
            _ptz.SetAutoFocus(PTZFacade.AutoFocus.On);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.SetZoomMagnification(20);
            Thread.Sleep(ONE_SECOND_MS * 10);
            _ptz.Focus(PTZFacade.FocusDirection.Far);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Focus(PTZFacade.FocusDirection.Near);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Focus(PTZFacade.FocusDirection.Stop);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.SetZoomMagnification(zoomMag);
        }

        /// <summary>
        /// Exercise the camera's actions relating to iris, zoom
        /// </summary>
        private static void ExerciseIris()
        {
            float zoomMag = _ptz.ZoomMagnification();
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.SetZoomMagnification(1);
            Thread.Sleep(ONE_SECOND_MS * 3);
            _ptz.SetAutoIris(PTZFacade.AutoIris.Off);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Zoom(PTZFacade.ZoomDirection.In);
            Thread.Sleep(ONE_SECOND_MS * 2);
            _ptz.Zoom(PTZFacade.ZoomDirection.Stop);
            Thread.Sleep(ONE_SECOND_MS * 10);
            _ptz.SetAutoIris(PTZFacade.AutoIris.On);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Zoom(PTZFacade.ZoomDirection.Out);
            Thread.Sleep(ONE_SECOND_MS * 2);
            _ptz.Zoom(PTZFacade.ZoomDirection.Stop);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Iris(PTZFacade.IrisDirection.Close);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Iris(PTZFacade.IrisDirection.Open);
            Thread.Sleep(ONE_SECOND_MS);
            _ptz.Iris(PTZFacade.IrisDirection.Stop);
            Thread.Sleep(ONE_SECOND_MS * 10);
            _ptz.SetZoomMagnification(zoomMag);
        }
    }
}
